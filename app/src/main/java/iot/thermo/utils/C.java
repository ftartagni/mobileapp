package iot.thermo.utils;

public class C {

    public static final String APP_LOG_TAG = "BT CLN";
    public static final String LIB_TAG = "BluetoothLib";

    public class bluetooth {
        public static final int ENABLE_BT_REQUEST = 1;
        /* Name of the bluetooth server receiver */
        public static final String BT_DEVICE_ACTING_AS_SERVER_NAME = "isi00";
    }

    public class channel {
        public static final int MESSAGE_RECEIVED = 0;
        public static final int MESSAGE_SENT = 1;
    }

    public class message {
        public static final char MESSAGE_TERMINATOR = '\n';
    }
}
