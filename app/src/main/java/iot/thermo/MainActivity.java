package iot.thermo;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import iot.thermo.exceptions.BluetoothDeviceNotFound;
import iot.thermo.utils.C;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MainActivity extends AppCompatActivity {

    private BluetoothChannel btChannel;
    private String temp;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }

        initUI();
        initNumberPicker();
    }

    private void initUI() {
        findViewById(R.id.connectBTbtn).setOnClickListener(l ->
            {
                try {
                    connectToBTServer();
                } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                    bluetoothDeviceNotFound.printStackTrace();
                }
            }
        );

        findViewById(R.id.sendTempBtn).setOnClickListener(l -> {
            btChannel.sendMessage(temp);
            //System.out.println("SEND: " + temp);
        });

        findViewById(R.id.dayButton).setOnClickListener(l -> btChannel.sendMessage("DAY"));

        findViewById(R.id.nightButton).setOnClickListener(l -> btChannel.sendMessage("NIGHT"));
    }

    private void initNumberPicker(){
        NumberPicker np = findViewById(R.id.numberPicker);
        List<Double> nums = new ArrayList<>();
        nums.addAll(Stream.iterate(0, n -> n + 1).limit(16).map(n -> n + 10.00).collect(Collectors.toList()));
        nums.addAll(nums.stream().map(n -> n + 0.5).limit(nums.size() - 1).collect(Collectors.toList()));
        Collections.sort(nums);
        List<String> str = nums.stream().map(n -> n.toString()).collect(Collectors.toList());
        String[] num = str.toArray(new String[0]);
        np.setMinValue(0);
        np.setMaxValue(num.length-1);
        np.setWrapSelectorWheel(false);
        np.setDisplayedValues(num);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setOnValueChangedListener((picker, oldVal, newVal) -> {
            //this.temp = Float.parseFloat(num[newVal]);
            this.temp = num[newVal];
            System.out.println(this.temp);
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        btChannel.close();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK) {
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED) {
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);

        // ! UTILIZZARE IL CORRETTO VALORE DI UUID
        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();
        //final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.selectLbl)).setText(String.format("Select profile, connected to: %s",
                        serverDevice.getName()));

                findViewById(R.id.connectBTbtn).setEnabled(false);

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        //((TextView) findViewById(R.id.sendTempBtn)).append(String.format("Temp: %s", receivedMessage));
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                        //((TextView) findViewById(R.id.profileTextView)).append(String.format("", sentMessage));
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.titleTextView)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }
}