package iot.thermo;

public interface ExtendedRunnable extends Runnable {
    void write(byte[] bytes);
    void cancel();
}
